
const _service = require('./keycloak-client-service');

module.exports.KeycloakServiceSettings = _service.KeycloakServiceSettings;
module.exports.KeycloakProfile = _service.KeycloakProfile;
module.exports.KeycloakClientService = _service.KeycloakClientService;
