const unirest = require('unirest');
const HttpError = require('@themost/common').HttpError;
const ApplicationService = require('@themost/common/app').ApplicationService;
const URL = require('url').URL;

/**
 * @property {string} sub (e.g. 00000000-0000-0000-0000-432730e4f6b2)
 * @property {string} name
 * @property {string} preferred_username
 * @property {string} sub
 * @property {string} given_name
 * @property {string} family_name
 * @property {string} email
 */
// eslint-disable-next-line no-unused-vars
class KeycloakProfile {

}
/**
 * @property {string} server_uri
 * @property {string} client_id
 * @property {string} client_secret
 * @property {string} profile_uri
 */
// eslint-disable-next-line no-unused-vars
class KeycloakServiceSettings {

}

/**
 * @class
 */
class KeycloakClientService extends ApplicationService {
    /**
     * @param {IApplication} app
     */
    constructor(app) {
        super(app);
        /**
         * @name KeycloakClientService#settings
         * @type {KeycloakServiceSettings}
         */
        Object.defineProperty(this, 'settings', {
            writable: false,
            value: app.getConfiguration().getSourceAt("settings/auth"),
            enumerable: false,
            configurable: false
        });
    }

    /**
     * Gets keycloak server root
     * @returns {string}
     */
    getServer() {
        return this.settings.server_uri;
    }

    // noinspection JSUnusedGlobalSymbols
    /**
     * Gets user profile by calling keycloak server profile endpoint
     * @param {*} context
     * @param {string} token
     */
    getProfile(context, token) {
        if (this.settings.profile_uri == null) {
            return Promise.reject(new Error('An application configuration setting is missing. User profile URI is not defined or is inaccessible.'))
        }
        if (token == null) {
            return Promise.reject(new Error('User token cannot be empty at this context.'));
        }
        const self = this;
        return new Promise(function(resolve, reject) {
            return unirest.get(new URL(self.settings.server_uri, self.settings.profile_uri))
                .header('Authorization', `Bearer ${token}`)
                .header('Accept', 'application/json')
                .end(function (response) {
                    const body =  response.body;
                    if (body && body.error) {
                        return reject(Object.assign(new HttpError(body.code || 500), body));
                    }
                    return resolve(body);
                });
        });
    }

    // noinspection JSUnusedGlobalSymbols
    /**
     * Gets the token info of the current context
     * @param {*} context
     */
    getContextTokenInfo(context) {
        if (context.user == null) {
            return Promise.reject(new Error('Context user may not be null'));
        }
        if (context.user.authenticationType !== 'Bearer') {
            return Promise.reject(new Error('Invalid context authentication type'));
        }
        if (context.user.authenticationToken == null) {
            return Promise.reject(new Error('Context authentication data may not be null'));
        }
        return this.getTokenInfo(context, context.user.authenticationToken);
    }

    /**
     * Gets token info by calling keycloak server endpoint
     * @param {*} context
     * @param {string} token
     */
    getTokenInfo(context, token) {
        return new Promise((resolve, reject)=> {
            return unirest.post(new URL('token/introspect', this.getServer()))
                .header('Authorization', 'Basic ' + new Buffer(this.settings.client_id + ':' + this.settings.client_secret).toString('base64'))
                .type('form')
                .send({
                    "token_type_hint":"access_token",
                    "token":token,
                }).end(function (response) {
                    if (response.error) {
                        if (response.clientError) {
                            return reject(new HttpError(response.error.status));
                        }
                        return reject(response.error);
                    }
                    if (response.body && response.body.scope) {
                        // replace space with comma (for backward compatibility)
                        response.body.scope = response.body.scope.replace(/\s/g,',');
                    }
                    return resolve(response.body);
                });
        });
    }

}

module.exports.KeycloakServiceSettings = KeycloakServiceSettings;
module.exports.KeycloakProfile = KeycloakProfile;
module.exports.KeycloakClientService = KeycloakClientService;
